#### #################################################################### ####
####      Inverse distance weighting for pollution levels                 #### 
#### #################################################################### ####
#### Distance between households and pollution stations
#### IDW between pollution monitors and households 
#### Simple descriptives
#### IDW data into wide format & export
#### #################################################################### ####

rm(list = ls()); gc()

library(tidyverse)
library(sf)
library(geosphere)
library(data.table)

####_____________________________________________________________________ ####
#### Distance between households and pollution stations                   ####
#### ____________________________________________________________________ ####

# --- SOEP geo data
overlay = st_read("/import/SOEP-GIS/soep/soep35/soep_hh_korr_mit_fakes_utm32-v35.shp")
head(overlay)

overlay = st_transform(overlay, crs = 4326)
overlay = rename(overlay, year = erhebj)

# --- Keep only years >= 2005
overlay = filter(overlay, year >= 2005)

# --- Add lat/lon based on geometry column
overlay = overlay %>% mutate(lat = st_coordinates(.)[,2],
                             lon = st_coordinates(.)[,1]) %>%
  st_set_geometry(NULL)

# --- Load AQI data
pol = readRDS(file = "/home/nwaegner/work/soep136_20220419/AQI_2005_2018.rds")

# --- Merge pollution monitor locations
stations = readRDS(file = "/home/nwaegner/work/soep136_20210125/pollution_data_2005_2018.rds") %>%
  distinct(station, longitude, latitude)
pol = left_join(pol, stations)

sapply(pol, function(x) length(which(is.na(x))))
length(which(duplicated(pol[, c("station", "date")])))

stations = distinct(pol, station, lon = longitude, lat = latitude)
rm("pol")

# --- Distance function
# df1 - data frame with household id (ID) + coordinates (lon, lat)
# df2 - data frame with pollution monitor id (station) + coordinates (lon, lat)
# limit - max. distance (in m) to be returned
distance_meters <- function(df1, df2, limit){ 
  distance <- distHaversine(dplyr::select(df1, lon, lat), 
                            dplyr::select(df2, lon, lat))
  df_distance <- data.table(station_id = dplyr::select(df2, station),
                            hh_id = dplyr::select(df1, ID),
                            distance = distance, 
                            lon_w = dplyr::select(df2, lon), 
                            lat_w = dplyr::select(df2, lat), 
                            lon_hh = dplyr::select(df1, lon),
                            lat_hh = dplyr::select(df1, lat))
  return(filter(df_distance, distance < limit))
}

# --- Calculate the distance between households and pollution monitors within 20 km distance
# Split by year and delete original df to save working memory and server space
table(overlay$year)
years = unique(overlay$year)
year_list = split(overlay, f = overlay$year)
rm("overlay")
gc()

# Loop over years
for(y in 1:length(year_list)){
  
  # Calculate distance to pollution monitors; store results for each household in a list; keep only distances < 20km
  distance_list = list()
  for(i in 1:nrow(year_list[[y]])){
    
    distance_list[[i]] = distance_meters(year_list[[y]][i,], stations, 20000)
    
    print(paste("Year", years[[y]], " Nr. ", i, " has been processed")) 
  }
  
  distance_stations = rbindlist(distance_list)
  colnames(distance_stations) = c("station_id", "hh_id","distance", "w_lon", "w_lat", "hh_lon", "hh_lat")
  
  saveRDS(distance_stations, file = paste0("/home/nwaegner/data/generated_data/distance_stations_", years[[y]], ".rds"))
  rm("distance_list", "distance_stations")
}

rm("year_list")
gc()

#### ____________________________________________________________________ ####
####       IDW between pollution monitors and households                  ####
#### ____________________________________________________________________ ####

library(tidyverse)
library(data.table)
library(readr)

# --- Load pollution data
pol = readRDS(file = "/home/nwaegner/work/soep136_20210317/AQI_2005_2018.rds")
table(pol$year, useNA = "ifany")

# --- Loop over years in overlay data (2005 - 2018)
years = 2000:2004
for(y in 1:length(years)){
  
  # Select only pollution dates in given year y
  pol_y = filter(pol, year == years[[y]])
  
  # Load distances between households and pollution monitors
  distance_stations <- readRDS(file = paste0("/home/nwaegner/data/generated_data/distance_stations_", years[[y]] , ".rds"))

  # Consider only pollution monitors within 20km distance
  distance_stations = filter(distance_stations, distance <= 20000)
  
  # Loop over hh/row IDs
  dist_list = split(distance_stations, f = distance_stations$hh_id)
  pol_list = vector("list", length(dist_list))
  for(i in 1:length(dist_list)){
    
    # Merge daily pollution data with distances
    pol_long = left_join(dist_list[[i]], pol_y, by = c("station_id" = "station"))

    # Create character vector of AQI variable names and binding pollutants
    variables = c("aqi_max", "aqi_mean")
    variables_pol = paste0(variables, "_pol")
      
    # Loop over AQI variables
    list_idw = list()
    for(v in 1:length(variables)){
      
      print(variables[[v]])
      
      # Run the IDW for each HH date combination
      df_idw = pol_long %>% ungroup() %>%
        filter(is.na(!!sym(variables[[v]])) == FALSE) %>%
        group_by(hh_id, date) %>%
        summarise(temp = sum((1 / distance^2) * !!sym(variables[[v]])) / sum(1 / distance^2), .groups = 'drop')
      
      colnames(df_idw) = c("hh_id", "date", as.character(variables[[v]]))
      
      # Add pollutant that is binding for daily AQI at the nearest pollution station (filter by min distance to station)
      df_nearest = pol_long %>% ungroup() %>%
        group_by(hh_id, date) %>%
        filter(is.na(!!sym(variables[[v]])) == FALSE,
               distance == min(distance)) %>%
        summarize(aqi_pol = !!sym(variables_pol[[v]]), .groups = 'drop')
      
      colnames(df_nearest) = c("hh_id", "date", as.character(variables_pol[[v]]))
      
      # Join IDW AQI values and binding pollutants, save in list
      df_idw = left_join(df_idw, df_nearest, by = c("hh_id", "date"))
        
      list_idw[[v]] = df_idw
    }
    names(list_idw) = variables
    
    pol_list[[i]] = plyr::join_all(list_idw, by=c("hh_id", "date"), type='full')
    
    print(paste("Year", years[[y]], " Nr. ", i, " out of ", length(dist_list), " has been processed")) 
  }
  
  idw = bind_rows(pol_list)
  
  # Check for duplicates
  idw = idw %>% group_by(hh_id, date) %>% mutate(dupl = n())
  print(table(idw$dupl, useNA = "ifany"))
  
  # Save IDW data frame for each year
  saveRDS(idw, file = paste0("/home/nwaegner/data/generated_data/idw20km_aqi_", years[[y]], ".rds"))
  rm("distance_stations", "dist_list", "df_idw", "df_nearest", "pol_list", "idw")
  gc()
  
}  
  
rm(list = ls())
gc()

#### ____________________________________________________________________ ####
####       IDW data into wide format & export                             ####
#### ____________________________________________________________________ ####

library(tidyverse)
library(stringr)
library(readr)

# General instruction for data transfer from Moran to Hauser
# http://companion.soep.de/Working%20with%20SOEP%20Data/Working%20with%20SOEP%20IGEL.html#data-transfer-from-moran-to-hauser

# --- Loop over years (2005 - 2018)
years = 2000:2004
list_years = vector("list", length(years))
for(y in 1:length(years)){
  
  # --- Load IDW pollution data
  idw <- readRDS(file = paste0("/home/nwaegner/data/generated_data/idw20km_aqi_", years[[y]], ".rds"))
  print(table(idw$dupl, useNA = "ifany"))
  
  # --- Recode identifiers id, year and day-of-the-year
  idw = data.frame(idw)
  idw = idw %>%
    mutate(year = lubridate::year(date),
           doy = strftime(date, format = "%j")) %>% 
    select(id = hh_id, year, doy, everything(), -dupl)
  head(idw)
  
  # --- From long to wide format 
  wide = pivot_wider(idw, id_cols = id:year, names_from = doy, values_from = aqi_max:aqi_mean_pol)
  head(wide)
  names(wide)
  
  # --- Check for duplicates
  print(sprintf("Unique row IDs in year %s: %s.", years[[y]], length((unique(wide$id)))))
  print(sprintf("Nr. of duplicated row IDs in year %s is %s.", years[[y]], length(which(duplicated(wide$id)))))
  
  # saveRDS(wide, file = paste0("/home/nwaegner/transfer/export/2021-01-28/pollution10km_wide_", years[[y]], ".rds"))
  list_years[[y]] <- wide
  rm("idw", "wide")
}
wide = data.table::rbindlist(list_years, fill = TRUE)
saveRDS(wide, file = paste0("/home/nwaegner/transfer/export/2021-03-24/aqi20km_wide_2005_2018.rds"))

rm(list = ls()); gc()

# --- Generate README.csv
wide = readRDS(file = paste0("/home/nwaegner/transfer/export/2021-03-24/aqi20km_wide_2005_2018.rds"))

df = data.frame(name = names(wide), description = rep("", length(names(wide))))

df$description[df$name == "id"] = "Row identifier (ID)"
df$description[df$name == "year"] = "Year (erhebj)"

df$description = ifelse(str_sub(df$name, 1 , 7) == "aqi_max", 
                        paste0("Maximum AQI on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)
df$description = ifelse(str_sub(df$name, 1 , 11) == "aqi_max_pol", 
                        paste0("Binding pollutant(s) of max. AQI on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)
df$description = ifelse(str_sub(df$name, 1 , 8) == "aqi_mean", 
                        paste0("Mean AQI on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)
df$description = ifelse(str_sub(df$name, 1 , 12) == "aqi_mean_pol", 
                        paste0("Binding pollutant(s) of mean AQI on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)

write.csv(df, file = "/home/nwaegner/transfer/export/2021-03-24/aqi20km_README.csv", row.names = F)
