#### #################################################################### ####
####      Inverse distance weighting for pollution levels                 #### 
#### #################################################################### ####
#### Distance between households and pollution stations
#### IDW between pollution monitors and households 
#### IDW data into wide format & export

rm(list = ls()); gc()

library(tidyverse)
library(sf)
library(geosphere)
library(data.table)

####_____________________________________________________________________ ####
#### Distance between households and pollution stations                   ####
#### ____________________________________________________________________ ####

# --- SOEP geo data
overlay = st_read("/import/SOEP-GIS/soep/soep35/soep_hh_korr_mit_fakes_utm32-v35.shp")
head(overlay)

overlay = st_transform(overlay, crs = 4326)
overlay = rename(overlay, year = erhebj)

# --- Keep only years >= 2005
overlay = filter(overlay, year >= 2005)

# --- Add lat/lon based on geometry column
overlay = overlay %>% mutate(lat = st_coordinates(.)[,2],
                             lon = st_coordinates(.)[,1]) %>%
  st_set_geometry(NULL)

# --- Load pollution monitor locations
pol = readRDS(file = "/home/nwaegner/work/soep136_20210125/pollution_data_2005_2018.rds")
stations = distinct(pol, station, lon = longitude, lat = latitude)
rm("pol")

# --- Distance function
# df1 - data frame with household id (ID) + coordinates (lon, lat)
# df2 - data frame with pollution monitor id (station) + coordinates (lon, lat)
# limit - max. distance (in m) to be returned
distance_meters <- function(df1, df2, limit){
  distance <- distHaversine(dplyr::select(df1, lon, lat),
                            dplyr::select(df2, lon, lat))
  df_distance <- data.table(station_id = dplyr::select(df2, station),
                            hh_id = dplyr::select(df1, ID),
                            distance = distance,
                            lon_w = dplyr::select(df2, lon),
                            lat_w = dplyr::select(df2, lat),
                            lon_hh = dplyr::select(df1, lon),
                            lat_hh = dplyr::select(df1, lat))
  return(filter(df_distance, distance <= limit))
}

# --- Calculate the distance between households and pollution monitors within 20 km distance
# Split by year and delete original df to save working memory and server space
table(overlay$year)
years = unique(overlay$year)
year_list = split(overlay, f = overlay$year)
rm("overlay")
gc()

# Loop over years
for(y in 1:length(year_list)){

  # Calculate distance to pollution monitors; store results for each household in a list
  distance_list = list()
  for(i in 1:nrow(year_list[[y]])){

    distance_list[[i]] = distance_meters(year_list[[y]][i,], stations, 20000)

    print(paste("Year", years[[y]], " Nr. ", i, " has been processed"))
  }

  distance_stations = rbindlist(distance_list)
  colnames(distance_stations) = c("station_id", "hh_id","distance", "w_lon", "w_lat", "hh_lon", "hh_lat")

  saveRDS(distance_stations, file = paste0("/home/nwaegner/data/generated_data/distance_stations_", years[[y]], ".rds"))
  rm("distance_list", "distance_stations")
}

rm("year_list")
gc()

#### ____________________________________________________________________ ####
####       IDW between pollution monitors and households                  ####
#### ____________________________________________________________________ ####

library(tidyverse)
library(data.table)
library(readr)

# --- Load pollution data
pol = readRDS(file = "/home/nwaegner/work/soep136_20210125/pollution_data_2005_2018.rds")
table(pol$year, useNA = "ifany")

# --- Run IDW only for HH ids not already exported onn Jan 28th
# These are HHs with missing values in CO (due to coding error, left_join instead of full_join)
exported = readRDS(file = "/home/nwaegner/transfer/export/2021-01-28/pollution20km_wide_2005_2018.rds") %>%
  distinct(id, year)

# --- Loop over years in overlay data (2005 - 2018)
years = 2005:2018
for(y in 1:length(years)){
  
  # Select only pollution dates in given year y
  pol_y = filter(pol, year == years[[y]])
  
  # Load distances between households and pollution monitors
  distance_stations <- readRDS(file = paste0("/home/nwaegner/data/generated_data/distance_stations_", years[[y]] , ".rds"))

  # Consider only pollution monitors within 10km / 20km distance
  distance_stations = filter(distance_stations, distance <= 20000)
  
  # Consider only HH ids that have not yet been exported on Jan 28th
  exported_y = filter(exported, year == years[[y]])
  distance_stations = filter(distance_stations, !(hh_id %in% exported_y$id))
  
  # Loop over hh/row IDs
  dist_list = split(distance_stations, f = distance_stations$hh_id)
  pol_list = vector("list", length(dist_list))
  for(i in 1:length(dist_list)){
    
    # Merge daily pollution data with distances
    pol_long = left_join(dist_list[[i]], pol_y, by = c("station_id" = "station"))

    # Create character vector of pollution variable names
    variables = names(dplyr::select(pol_long, co:so2))

    # Run the IDW; loop over pollution variables
    list_idw = list()
    for(v in 1:length(variables)){
      list_idw[[v]] = pol_long %>% ungroup() %>%
        filter(is.na(!!sym(variables[[v]])) == FALSE) %>%
        group_by(hh_id, date) %>%
        summarise(temp = sum((1 / distance^2) * !!sym(variables[[v]])) / sum(1 / distance^2), .groups = 'drop')
      colnames(list_idw[[v]]) = c("hh_id", "date", as.character(variables[[v]]))
      print(variables[[v]])
    }
    names(list_idw) = variables
    
    pol_list[[i]] = plyr::join_all(list_idw, by=c("hh_id", "date"), type='full')
    
    print(paste("Year", years[[y]], " Nr. ", i, " out of ", length(dist_list), " has been processed")) 
  }
  
  idw = bind_rows(pol_list)
  
  # Check for duplicates
  idw = idw %>% group_by(hh_id, date) %>% mutate(dupl = n())
  print(table(idw$dupl, useNA = "ifany"))
  
  # Save IDW data frame for each year
  saveRDS(idw, file = paste0("/home/nwaegner/data/generated_data/idw20km_pol_", years[[y]], ".rds"))
  rm("pol_y", "exported_y", "distance_stations", "dist_list", "pol_list", "idw", "list_idw")
  gc()
  
}  
  
rm(list = ls())
gc()

#### ____________________________________________________________________ ####
####       Simple descriptives                                            ####
#### ____________________________________________________________________ ####

# --- Function for simple descriptives
dstat <- function(X,d){
  
  X <- as.matrix(X)
  mat <- matrix(NA, ncol=13, nrow=ncol(X))
  colnames(mat) <- c("Min","Q5","Q25","Med","Mean","Q75","Q95","Max", "SD", "Var",
                     "Zeros", "NAs","notNAs")
  rownames(mat) <- colnames(X)
  mat[,1] <- round(apply(X, 2, min, na.rm=T), digits=d)
  mat[,2] <- round(apply(X, 2, quantile, probs=0.05, na.rm=T),digits=d)
  mat[,3] <- round(apply(X, 2, quantile, probs=0.25, na.rm=T),digits=d)
  mat[,4] <- round(apply(X, 2, median, na.rm=T),digits=d)
  mat[,5] <- round(apply(X, 2, mean, na.rm=T),digits=d)
  mat[,6] <- round(apply(X, 2, quantile, probs=0.75, na.rm=T),digits=d)
  mat[,7] <- round(apply(X, 2, quantile, probs=0.95, na.rm=T),digits=d)
  mat[,8] <- round(apply(X, 2, max, na.rm=T), digits=d)
  mat[,9] <- round(apply(X, 2, sd, na.rm=T), digits=d)
  mat[,10] <- round(apply(X, 2, var,na.rm=T),digits=d)
  for (i in 1:ncol(X)) {mat[i,11] <- length(which(X[,i]==0))[1]}
  for (i in 1:ncol(X)) {mat[i,12] <- length(which(is.na(X[,i])))[1]}
  for (i in 1:ncol(X)) {mat[i,13] <- length(which(!is.na(X[,i])))[1]}
  return(mat)
}

# --- Year 2006
idw <- readRDS(file = paste0("/home/nwaegner/data/generated_data/idw20km_pol_2006.rds"))
pols = c("co","no","no2","o3","pm10","pm25","so2")

# --- Simple descriptives: Min, max, mean, NAs etc.
stats_06 = dstat(idw[, c(pols)], 4)
stats_06 = stats_06[, c("Mean", "SD", "Min", "Max", "notNAs")]
stats_06

# --- Year 2016
idw <- readRDS(file = paste0("/home/nwaegner/data/generated_data/idw20km_pol_2016.rds"))

# --- Simple descriptives: Min, max, mean, NAs etc.
stats_16 = dstat(idw[, c(pols)], 4)
stats_16 = stats_16[, c("Mean", "SD", "Min", "Max", "notNAs")]
stats_16

# --- Year 2018
idw <- readRDS(file = paste0("/home/nwaegner/data/generated_data/idw20km_pol_2018.rds"))

# --- Simple descriptives: Min, max, mean, NAs etc.
stats_18 = dstat(idw[, c(pols)], 4)
stats_18 = stats_18[, c("Mean", "SD", "Min", "Max", "notNAs")]
stats_18

rm(list = ls())
gc()

#### ____________________________________________________________________ ####
####       IDW data into wide format & export                             ####
#### ____________________________________________________________________ ####

library(tidyverse)
library(stringr)
library(readr)

# --- Loop over years (2005 - 2018)
years = 2005:2018
list_years = vector("list", length(years))
for(y in 1:length(years)){
  
  # --- Load IDW pollution data
  idw <- readRDS(file = paste0("/home/nwaegner/data/generated_data/idw20km_pol_", years[[y]], ".rds"))  %>%
    select(-co) # CO only with missing values due to left_join error

  print(table(idw$dupl, useNA = "ifany"))
  
  # --- Recode identifiers id, year and day-of-the-year
  idw = data.frame(idw)
  idw = idw %>%
    mutate(year = lubridate::year(date),
           doy = strftime(date, format = "%j")) %>% 
    select(id = hh_id, year, doy, everything(), -dupl)
  head(idw)
  
  # --- From long to wide format 
  wide = pivot_wider(idw, id_cols = id:year, names_from = doy, values_from = no:so2)
  head(wide)
  names(wide)
  
  # --- Check for duplicates
  print(sprintf("Unique row IDs in year %s: %s.", years[[y]], length((unique(wide$id)))))
  print(sprintf("Nr. of duplicated row IDs in year %s is %s.", years[[y]], length(which(duplicated(wide$id)))))
  
  list_years[[y]] <- wide
  rm("idw", "wide")
}
wide = rbindlist(list_years, fill = TRUE)
saveRDS(wide, file = paste0("/home/nwaegner/transfer/export/2021-07-07/pollution20km_wide_2005_2018_addendum.rds"))

rm(list = ls()); gc()


# --- Generate README.csv
wide = readRDS(file = paste0("/home/nwaegner/transfer/export/2021-07-07/pollution20km_wide_2005_2018_addendum.rds"))

df = data.frame(name = names(wide), description = rep("", length(names(wide))))

df$description[df$name == "id"] = "Row identifier (ID)"
df$description[df$name == "year"] = "Year (erhebj)"

df$description = ifelse(str_sub(df$name, 1 , 2) == "co", 
                        paste0("Carbon monoxide level on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)
df$description = ifelse(str_sub(df$name, 1 , 2) == "no", 
                        paste0("Nitric oxide level on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)
df$description = ifelse(str_sub(df$name, 1 , 3) == "no2", 
                        paste0("Nitrogen dioxide level on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)
df$description = ifelse(str_sub(df$name, 1 , 2) == "o3", 
                        paste0("Ozone level on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)
df$description = ifelse(str_sub(df$name, 1 , 4) == "pm10", 
                        paste0("Coarse particulate matter on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)
df$description = ifelse(str_sub(df$name, 1 , 4) == "pm25", 
                        paste0("Fine particulate matter on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)
df$description = ifelse(str_sub(df$name, 1 , 3) == "so2", 
                        paste0("Sulfur dioxide level on doy nr.", str_sub(df$name, -3, -1)),
                        df$description)

write.csv(df, file = "/home/nwaegner/transfer/export/2021-07-07/README.csv")
